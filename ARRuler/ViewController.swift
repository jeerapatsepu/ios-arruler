//
//  ViewController.swift
//  ARRuler
//
//  Created by Jeerapat Sripumngoen on 30/6/2563 BE.
//  Copyright © 2563 Jeerapat Sripumngoen. All rights reserved.
//

import UIKit
import SceneKit
import ARKit

class ViewController: UIViewController, ARSCNViewDelegate {

    @IBOutlet var sceneView: ARSCNView!
    
    var dots = [SCNNode]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Set the view's delegate
        sceneView.delegate = self
        
//        // Show statistics such as fps and timing information
//        sceneView.showsStatistics = true
//
//        // Create a new scene
//        let scene = SCNScene(named: "art.scnassets/ship.scn")!
//
//        // Set the scene to the view
//        sceneView.scene = scene
        
        sceneView.debugOptions = [ARSCNDebugOptions.showFeaturePoints]
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        // Create a session configuration
        let configuration = ARWorldTrackingConfiguration()

        // Run the view's session
        sceneView.session.run(configuration)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        // Pause the view's session
        sceneView.session.pause()
    }

    // MARK: - ARSCNViewDelegate
    
/*
    // Override to create and configure nodes for anchors added to the view's session.
    func renderer(_ renderer: SCNSceneRenderer, nodeFor anchor: ARAnchor) -> SCNNode? {
        let node = SCNNode()
     
        return node
    }
*/
    
    func session(_ session: ARSession, didFailWithError error: Error) {
        // Present an error message to the user
        
    }
    
    func sessionWasInterrupted(_ session: ARSession) {
        // Inform the user that the session has been interrupted, for example, by presenting an overlay
        
    }
    
    func sessionInterruptionEnded(_ session: ARSession) {
        // Reset tracking and/or remove existing anchors if consistent tracking is required
        
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        print("touch")
        
        if let touch = touches.first {
            
            let touchLocation = touch.location(in: sceneView)
            
            let results = sceneView.hitTest(touchLocation, types: .featurePoint)
            
            if let result = results.first {
                
                addDot(withResult: result)
                
            }
            
        }
    
    }
    
    func addDot(withResult result: ARHitTestResult) {
        
        guard dots.count < 2 else {
            return
        }
        
        let dot = SCNSphere(radius: 0.005)
        
        let material = SCNMaterial()
        
        material.diffuse.contents = UIColor.red
        
        dot.materials = [material]
        
        let node = SCNNode(geometry: dot)
        
//        print(result)
        
        node.position = SCNVector3(CGFloat(result.worldTransform.columns.3.x), CGFloat(result.worldTransform.columns.3.y), CGFloat(result.worldTransform.columns.3.z))
        
        sceneView.scene.rootNode.addChildNode(node)
        
        sceneView.autoenablesDefaultLighting = true
        
//        if dots.count < 2 {
            
            dots.append(node)
            
        if dots.count >= 2 {
            calcDistance()
        }
            
//        }
        
    }
    
    func calcDistance() {
        
        let firstNode = dots[0]
        let secondNode = dots[1]
        
        let x = secondNode.position.x.rounded() - firstNode.position.x.rounded()
        let y = secondNode.position.y.rounded() - firstNode.position.y.rounded()
        let z = secondNode.position.z.rounded() - firstNode.position.z.rounded()
        
        var calculated = sqrt(pow(x, x) + pow(y, y) + pow(z, z))
        
        print(String(calculated.rounded()))
        
        showText3D(text: String(calculated), atLocation: secondNode.position)
    }
    
    func showText3D(text: String, atLocation location: SCNVector3) {
        let text3D = SCNText(string: text, extrusionDepth: 1)
        
        text3D.firstMaterial?.diffuse.contents = UIColor.red
        
        let node = SCNNode(geometry: text3D)
        
        node.position = SCNVector3(location.x, location.y, location.z)
        
        node.scale = SCNVector3(0.01, 0.01, 0.01)
        
        sceneView.scene.rootNode.addChildNode(node)
        
        sceneView.autoenablesDefaultLighting = true
        
    }
    
//    func renderer(_ renderer: SCNSceneRenderer, didAdd node: SCNNode, for anchor: ARAnchor) {
//
//        let planeARAnchor = anchor as! ARPlaneAnchor
//
//        let plane = SCNPlane(width: CGFloat(planeARAnchor.extent.x), height: CGFloat(planeARAnchor.extent.z))
//
////        A structural element of a scene graph, representing a position and transform in a 3D coordinate space, to which you can attach geometry, lights, cameras, or other displayable content.
//        let planeNode = SCNNode()
//
//        planeNode.position = SCNVector3(planeARAnchor.center.x, <#T##y: CGFloat##CGFloat#>, <#T##z: CGFloat##CGFloat#>)
//
//    }
}
